/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JDesktopPane;

/**
 *
 * @author domem
 */
public class DesktopPane extends JFrame 
{

    public DesktopPane() 
    {
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);

        setTitle("JDesktopPane Example");
        setSize(300, 350);
        setVisible(true);
    }
    public static void main(String[] args) 
    {
        DesktopPane frame = new DesktopPane();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
class CustomDesktopPane extends JDesktopPane 
{

    int numFrame = 3, x = 30, y = 30;

    public void display(CustomDesktopPane dp) 
    {
        for (int i = 0; i < numFrame; i++) 
        {
            JInternalFrame jframe = new JInternalFrame("Internal Frame" + i, true, true, true, true);

            jframe.setBounds(x, y, 250, 85);
            Container c1 = jframe.getContentPane();
            c1.add(new JLabel("I hate my country"));
            dp.add(jframe);
            jframe.setVisible(true);
            y += 85;
        }
    }
}

