/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.JFrame;
import javax.swing.JCheckBox;
/**
 *
 * @author domem
 */
public class CheckBoxFrame extends JFrame{
    JCheckBox chkBox1;
    JCheckBox chkBox2;
    CheckBoxFrame(){
        JFrame frame = new JFrame();
        chkBox1 = new JCheckBox("Student",true);
        chkBox1.setBounds(100, 100, 80, 50);
        chkBox2 = new JCheckBox("Teacher");
        chkBox2.setBounds(100, 150, 80, 50);
        frame.add(chkBox1);
        frame.add(chkBox2);
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new CheckBoxFrame();
    }
}
