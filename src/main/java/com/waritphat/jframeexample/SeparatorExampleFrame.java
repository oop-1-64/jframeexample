/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;
/**
 *
 * @author domem
 */
public class SeparatorExampleFrame extends JFrame{
    JLabel lbl1, lbl2;
    JSeparator sep;
    SeparatorExampleFrame(){
        JFrame frame = new JFrame();
        frame.setLayout(new GridLayout(0,1));
        lbl1 = new JLabel("Above Separator");
        frame.add(lbl1);
        sep = new JSeparator();
        frame.add(sep);
        lbl2 = new JLabel("Below Separator");
        frame.add(lbl2);
        frame.setSize(400,300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
    
    public static void main(String[] args) {
        new SeparatorExampleFrame();
    }
    
}
