/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.JComboBox;
import javax.swing.JFrame;
/**
 *
 * @author domem
 */
public class ComboBoxFrame extends JFrame{
    JComboBox cmbBox;
    ComboBoxFrame(){
        JFrame frame = new JFrame();
        String country[] = {"India", "China", "U.S.A", "England", "Newzealand"};
        cmbBox = new JComboBox(country);
        cmbBox.setBounds(50, 50, 100, 20);
        frame.add(cmbBox);
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new ComboBoxFrame();
    }
}
