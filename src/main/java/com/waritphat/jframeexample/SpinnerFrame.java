/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author domem
 */
public class SpinnerFrame extends JFrame {

    JFrame frame;
    JLabel lbl;
    JSpinner spinner;
    SpinnerModel value;

    SpinnerFrame() {
        frame = new JFrame();
        lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(250, 100);
        value = new SpinnerNumberModel(5, 0, 10, 1);
        spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        frame.add(spinner);
        frame.add(lbl);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        spinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                lbl.setText("Value: " + ((JSpinner) e.getSource()).getValue());
            }

        });

    }
    public static void main(String[] args) {
        new SpinnerFrame();
    }
}
