/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;

/**
 *
 * @author domem
 */
public class RootPaneFrame extends JFrame{
    JFrame frame;
    JRootPane root;
    JMenuBar bar;
    JMenu menu;
    
    RootPaneFrame(){
        frame = new JFrame();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        root = frame.getRootPane();
        
        bar = new JMenuBar();
        menu = new JMenu("File");
        bar.add(menu);
        menu.add("Open");
        menu.add("Close");
        root.setJMenuBar(bar);
        
        root.getContentPane().add(new JButton ("Press ME"));
        
        frame.pack();
        frame.setVisible(true);
        
    }
    public static void main(String[] args) {
        new RootPaneFrame();
    }
}
