/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
/**
 *
 * @author domem
 */
public class MenuExample extends JFrame{
    JMenu menu,submenu;
    JMenuItem i1,i2,i3;
    JMenuBar mb;
    MenuExample(){
        JFrame frame = new JFrame();
        mb = new JMenuBar();
        menu = new JMenu("File");
        submenu = new JMenu("Import");
        i1 = new JMenuItem("New file");
        i2 = new JMenuItem("Open file");
        i3 = new JMenuItem("From Zip");
        menu.add(i1);
        menu.add(i2);
        submenu.add(i3);
        menu.add(submenu);
        mb.add(menu);
        frame.setJMenuBar(mb);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new MenuExample();
    }
}
