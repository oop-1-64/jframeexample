/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 *
 * @author domem
 */
public class DisplayGraphicsFrame extends Canvas{
    public void paint(Graphics g){
        g.drawString("Hello", 40, 40);
        setBackground(Color.BLACK);
        g.fillRect(130, 30, 100, 80);
        g.drawOval(30, 130, 50, 60);
        setForeground(Color.RED);
        g.fillOval(130, 130, 50, 60);
        g.drawArc(30, 200, 40, 50, 90, 60);
        g.fillArc(30, 130, 40, 50, 180, 40);
    }
    public static void main(String[] args) {
        DisplayGraphicsFrame m = new DisplayGraphicsFrame();
        JFrame frame = new JFrame();
        frame.add(m);
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
