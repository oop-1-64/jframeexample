/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author domem
 */
public class EditorPaneFrame extends JFrame {

    JFrame frame = null;
    JEditorPane myPane;

    public static void main(String[] a) {
        (new EditorPaneFrame()).test();
    }

    private void test() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(400, 200);
        myPane = new JEditorPane();
        myPane.setContentType("test/plain");
        myPane.setText("Sleeping is necessary for a healthy body."  
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."  
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");
        frame.setContentPane(myPane);
        frame.setVisible(true);
    }
}
