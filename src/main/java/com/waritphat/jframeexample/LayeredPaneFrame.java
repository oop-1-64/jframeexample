/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;

/**
 *
 * @author domem
 */
public class LayeredPaneFrame extends JFrame{
    JLayeredPane pane;
    JButton btn_top,btn_mid, btn_bot;
    
    public LayeredPaneFrame(){
        super("LayeredPane Example");
        setSize(200,200);
        pane = getLayeredPane();
        btn_top = new JButton();
        btn_top.setBackground(Color.white);
        btn_top.setBounds(20, 20, 50, 50);
        btn_mid = new JButton();
        btn_mid.setBackground(Color.red);
        btn_mid.setBounds(40, 40, 50, 50);
        btn_bot = new JButton();
        btn_bot.setBackground(Color.cyan);
        btn_bot.setBounds(60, 60, 50, 50);
        pane.add(btn_top, new Integer(1));
        pane.add(btn_mid, new Integer(2));
        pane.add(btn_bot, new Integer(3));
    }
    public static void main(String[] args) {
        LayeredPaneFrame panel = new LayeredPaneFrame();
        panel.setVisible(true);
        panel.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
