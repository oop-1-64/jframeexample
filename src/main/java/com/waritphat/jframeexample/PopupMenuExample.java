/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author domem
 */
public class PopupMenuExample extends JFrame {

    JPopupMenu popup = new JPopupMenu("Edit");
    JMenuItem cut, copy, paste;

    PopupMenuExample() {
        JFrame frame = new JFrame();
        cut = new JMenuItem("Cut");
        copy = new JMenuItem("Copy");
        paste = new JMenuItem("Paste");
        popup.add(cut);
        popup.add(copy);
        popup.add(paste);
        frame.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                popup.show(frame, e.getX(), e.getY());
            }
        });
        frame.add(popup);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new PopupMenuExample();
    }
}
