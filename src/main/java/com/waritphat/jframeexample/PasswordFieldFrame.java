/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
/**
 *
 * @author domem
 */
public class PasswordFieldFrame extends JFrame{
    JPasswordField passField;
    JLabel lblPassword;
    PasswordFieldFrame(){
        JFrame frame = new JFrame();
        lblPassword = new JLabel("Password: ");
        lblPassword.setBounds(20, 100, 80, 30);
        passField = new JPasswordField();
        passField.setBounds(100, 100, 100, 30);
        frame.add(passField);
        frame.add(lblPassword);
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new PasswordFieldFrame();
    }
}
