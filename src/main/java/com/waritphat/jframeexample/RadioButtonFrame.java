/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.JRadioButton;
import javax.swing.JFrame;
/**
 *
 * @author domem
 */
public class RadioButtonFrame extends JFrame{
    JRadioButton radioBtn1;
    JRadioButton radioBtn2;
    RadioButtonFrame(){
        JFrame frame = new JFrame();
        radioBtn1 = new JRadioButton("Male");
        radioBtn2 = new JRadioButton("Female");
        radioBtn1.setBounds(75, 50, 100, 30);
        radioBtn2.setBounds(75, 100, 100, 30);
        frame.add(radioBtn1);
        frame.add(radioBtn2);
         frame.setSize(300, 300);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new RadioButtonFrame();
    }
    
}
