/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author domem
 */
public class DialogFrame extends JFrame {

    JFrame frame;
    private static JDialog d;
    JButton btn;

    DialogFrame() {
        frame = new JFrame();
        d = new JDialog(frame, "Dialog Example", true);
        d.setLayout(new FlowLayout());
        btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DialogFrame.d.setVisible(false);

            }
        });
        d.add(new JLabel("Click button to continue"));
        d.add(btn);
        d.setSize(300, 300);
        d.setVisible(true);
    }

    public static void main(String[] args) {
        new DialogFrame();
    }

}
