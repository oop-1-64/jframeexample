/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

/**
 *
 * @author domem
 */
public class ViewportFrame extends JFrame {

    JFrame frame;
    JLabel lbl;
    JScrollPane slp;
    JButton btn;

    ViewportFrame() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);

        lbl = new JLabel("Label");
        lbl.setPreferredSize(new Dimension(1000, 1000));
        slp = new JScrollPane(lbl);

        btn = new JButton();
        slp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        slp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        slp.setViewportBorder(new LineBorder(Color.RED));
        slp.getViewport().add(btn, null);

        frame.add(slp, BorderLayout.CENTER);
        frame.setSize(400, 150);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new ViewportFrame();
    }
}
