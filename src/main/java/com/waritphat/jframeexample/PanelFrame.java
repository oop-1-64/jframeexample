/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 *
 * @author domem
 */
public class PanelFrame extends JFrame{
    JFrame frame;
    JPanel panel;
    JButton btn1,btn2;
    
    PanelFrame(){
        frame = new JFrame();
        panel = new JPanel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.gray);
        btn1 = new JButton("Button 1");
        btn1.setBounds(50, 100, 80, 30);
        btn1.setBackground(Color.red);
        btn2 = new JButton("Button 2");
        btn2.setBounds(50, 100, 80, 30);
        btn2.setBackground(Color.green);
        panel.add(btn1);
        panel.add(btn2);
        frame.add(panel);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new PanelFrame();
    }
}
