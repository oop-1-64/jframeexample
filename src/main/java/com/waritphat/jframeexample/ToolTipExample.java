/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author domem
 */
public class ToolTipExample extends JFrame {

    JFrame frame;
    JPasswordField value;
    JLabel lbl;

    public ToolTipExample() {
        frame = new JFrame();
        value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("Enter your Password");
        lbl = new JLabel("Password");
        lbl.setBounds(20, 100, 80, 30);
        
        frame.add(value);
        frame.add(lbl);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new ToolTipExample();
    }

}
