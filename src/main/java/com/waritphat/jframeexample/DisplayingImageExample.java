/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author domem
 */
public class DisplayingImageExample extends Canvas{
    public void paint(Graphics g){
        
        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("your image.gif");
        g.drawImage(i, 120, 100, this);
    }
    public static void main(String[] args) {
        DisplayingImageExample m = new DisplayingImageExample();
        JFrame frame = new JFrame();
        frame.add(m);
        frame.setSize(400,400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
