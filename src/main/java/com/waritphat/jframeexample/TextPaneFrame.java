/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author domem
 */
public class TextPaneFrame extends JFrame{
    JFrame frame;
    Container cp;
    JTextPane pane;
    SimpleAttributeSet attSet;
    Document doc;
    JScrollPane slp;
    
    TextPaneFrame() throws BadLocationException{
        frame = new JFrame();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        cp = frame.getContentPane();
        pane = new JTextPane();
        attSet = new SimpleAttributeSet();
        StyleConstants.setBold(attSet, true);
        
        pane.setCharacterAttributes(attSet, true);
        pane.setText("Welcome");
        
        attSet = new SimpleAttributeSet();
        StyleConstants.setItalic(attSet, true);
        StyleConstants.setForeground(attSet, Color.red);
        StyleConstants.setBackground(attSet, Color.blue);
        
        doc = pane.getStyledDocument();
        doc.insertString(doc.getLength(), "to Java", attSet);
        
        attSet = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), "World", attSet);
        
        slp = new JScrollPane(pane);
        cp.add(slp, BorderLayout.CENTER);
        
        frame.setSize(400,300);
        frame.setVisible(true);
    }
    public static void main(String[] args) throws BadLocationException {
       new TextPaneFrame();
    }
}
