/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author domem
 */
public class ColorChooserClass extends JFrame implements ActionListener{

    JFrame frame;
    JButton btn;
    JTextArea txt;

    public ColorChooserClass() {
        frame = new JFrame();
        btn = new JButton("Pad Color");
        btn.setBounds(200, 250, 100, 30);
        txt = new JTextArea();
        txt.setBounds(10, 10, 300, 200);
        btn.addActionListener(this);
        frame.add(btn);
        frame.add(txt);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);
        txt.setBackground(c);
    }

    public static void main(String[] args) {
        new ColorChooserClass();
    }

}
