/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
/**
 *
 * @author domem
 */
public class TableFrame extends JFrame{
    JTable tbData;
    JScrollPane sp;
    TableFrame(){
        JFrame frame = new JFrame();
        String data[][] = {{"001", "Kiti" ,"50,000"},{"002", "Warit", "45,000"},{"003", "Bankky", "35,000"}};
        String column[] = {"ID", "NAME" , "SALARY"};
        tbData = new JTable(data,column);
        tbData.setBounds(30, 40, 200, 300);
        sp = new JScrollPane(tbData);
        frame.add(sp);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(300, 400);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new TableFrame();
    }
}
