/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author domem
 */
public class TabbedPaneFrame extends JFrame {

    JFrame frame;
    JTextArea txt;
    JPanel p1, p2, p3;
    JTabbedPane tp;

    public TabbedPaneFrame() {
        frame = new JFrame();
        txt = new JTextArea(200, 200);
        p1 = new JPanel();
        p1.add(txt);
        p2 = new JPanel();
        p3 = new JPanel();
        tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 200);
        tp.add("main", p1);
        tp.add("visit", p2);
        tp.add("help", p3);
        frame.add(tp);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new TabbedPaneFrame();
    }

}
