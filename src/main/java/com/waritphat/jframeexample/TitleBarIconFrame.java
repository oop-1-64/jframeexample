/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author domem
 */
public class TitleBarIconFrame extends JFrame{
    JFrame frame;
    Image icon;
    public TitleBarIconFrame(){
        frame = new JFrame();
        icon = Toolkit.getDefaultToolkit().getImage("My Image");
        
        frame.setIconImage(icon);
        frame.setLayout(null);
        frame.setSize(300, 300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new TitleBarIconFrame();
    }
    
}
