/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JFrame;
/**
 *
 * @author domem
 */
public class ListFrame extends JFrame{
    JList lsFootball;
    ListFrame(){
        JFrame frame = new JFrame();
        DefaultListModel<String> lsFootball = new DefaultListModel<>();
        lsFootball.addElement("Man city");
        lsFootball.addElement("Man U");
        lsFootball.addElement("Chelsea");
        lsFootball.addElement("Liverpool");
        JList<String> list = new JList<>(lsFootball);
        list.setBounds(100, 100, 75, 75);
        frame.add(list);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);

    }
    public static void main(String[] args) {
        new ListFrame();
    }
}
