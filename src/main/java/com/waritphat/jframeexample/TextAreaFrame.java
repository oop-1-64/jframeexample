/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author domem
 */
public class TextAreaFrame extends JFrame{
    JTextArea txtArea;
    TextAreaFrame(){
        JFrame frame = new JFrame();
        txtArea = new JTextArea("Name: Waritphat Kheereerat");
        txtArea.setBounds(10,30,200,200);
        frame.add(txtArea);
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new TextAreaFrame();
    }
}
