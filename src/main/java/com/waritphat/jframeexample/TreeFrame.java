/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author domem
 */
public class TreeFrame extends JFrame {

    JFrame frame;
    DefaultMutableTreeNode style, color, font, red, blue, black, green;
    JTree jt;

    TreeFrame() {
        frame = new JFrame();
        style = new DefaultMutableTreeNode("Style");
        color = new DefaultMutableTreeNode("color");
        font = new DefaultMutableTreeNode("font");
        style.add(color);
        style.add(font);
        red = new DefaultMutableTreeNode("red");
        blue = new DefaultMutableTreeNode("blue");
        black = new DefaultMutableTreeNode("black");
        green = new DefaultMutableTreeNode("green");
        color.add(red);
        color.add(blue);
        color.add(black);
        color.add(green);
        jt = new JTree(style);
        frame.add(jt);
        frame.setSize(400, 300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new TreeFrame();
    }
}
