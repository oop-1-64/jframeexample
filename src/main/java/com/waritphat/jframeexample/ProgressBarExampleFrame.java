/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.jframeexample;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author domem
 */
public class ProgressBarExampleFrame extends JFrame {

    JProgressBar jb;
    int i = 0, num = 0;

    ProgressBarExampleFrame() {
        JFrame frame = new JFrame();
        jb = new JProgressBar(0, 2000);
        jb.setBounds(40, 40, 160, 30);
        jb.setValue(0);
        jb.setStringPainted(true);
        frame.add(jb);
        frame.setSize(300, 200);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void iterate() {
        while (i <= 2000) {
            jb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
       ProgressBarExampleFrame m = new ProgressBarExampleFrame();
       m.iterate();
    }
}
